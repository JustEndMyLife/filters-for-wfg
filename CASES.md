## [CASE ONE]  
**TL:DR**  A sperg or group of spergs that break anonymity by sameposting, having exteremly off opinons for the sake of attention and avatarposting    
  
[Overview](https://i.imgur.com/XwqV9J3.jpg)   
  
These are just general purpose filters to clean /wfg/ up.  
The people that make these types of posts eventually take their leave if moderators step in.   
  - Stop replying to their posts    
  - Do not talk about their prevalence     
  - If you do quote their post with fake (you)s be wary that this breaks the filter chains    
    
    
    
    
        
## [CASE TWO]
**TL:DR** A sperg that tried to shove a biased tierlist down the general throat followed by sameposting, personal opinion spam no one agrees with and avatarposting  

[Who is tierqueer/TQ/BPNPC](http://puu.sh/pB7eV/bd126cd038.png) | [Rule Break Picture](http://i.imgur.com/rJIO7ty.jpg) | ["Only my opinions matter"](http://puu.sh/rdu5f/98e2313534.jpg)  
[Sameposting](http://i.imgur.com/XVzsYpK.png)  | [How bad can it get?](https://spit.mixtape.moe/view/raw/849e3190) | [Want to learn more?](https://spit.mixtape.moe/view/raw/11326972)  
/qa/ threads: [Thread 1](http://desuarchive.org/qa/thread/561868) |  [Thread 2](http://desuarchive.org/qa/thread/578206) 
  
  - Watch out for bait  [example 1](http://i.imgur.com/mXNNt7O.jpg) | [example 2](http://puu.sh/p1VGT/76d563e11a.jpg) | [example 3](https://i.imgur.com/IVXJaNO.png) 
  - Avoid screenshots with 50+ friend request, a user with 35k plat, 1920x1080 screenshots with the chatbox in the top left corner 
  - Avoid meaningless aurgments that seems to keep getting dragged through consecutive threads  
  - Avoid post pictures with obsessive amounts of underscores and/or shitty anime girls  
  - He is willing to call himself 'TQ' and samepost his own posts even after hours of it being posted  
  