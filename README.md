### Filters For /wfg/  
#### Zero Tolerance Edition    
_______________  
Quick Info:  
All filters are combined for conveniency.  
If you want to be selective, sections of the filters are labeled CASE and given a number.  
Scroll all the way down if you want to know which CASE set of filters you require.   
The filters' job is to negate derailment of /wfg/ general threads from both:

>**The Cause:**  "The source of the problem"  
>**The Echo:**  "Whether it's Negative or ironic Positive these post will signal boost 'The Cause'"  

These were designed to ONLY work on /vg/. remove `;boards:vg` from the filter to use else where if you find them useful for other things.    

_______________________________________  
|  
_______________________________________  
   
### ==4CHAN X== Comments, Filename and MD5 Filters Have Been Updated  
You can copy from the button or you can just select all and copypaste   
 ][4chanX site](https://www.4chan-x.net/)     
 ]Follow the directions and install the script    
 ]Go to Settings>Filter>Dropdown menu where it says 'guide' OR you can [Follow this](https://my.mixtape.moe/lppzku.webm)    
 ]Then you just copy and paste    
  *note: To use the copy button on ideone it requires you to allow adobe flash (hopefully they change that)  
          
## ------> [Comment Filters CLICK HERE](http://ideone.com/jrWy5c)  <--------------  
## ------> [Filenames Filters CLICK HERE](http://ideone.com/FplO0M)  <--------------  
## ------> [MD5Hash     Filters CLICK HERE](http://ideone.com/D8li5A)  <--------------  
  

|  
|    
### ==NON 4CHAN X and MOBILE==   (OUT OF DATE)  
Just copy and paste each line into the little box labeled pattern  
make sure you change what type of expression you are filtering 
by default it's tripcodes. Or you can [Follow this](http://i.imgur.com/XWxuSJy.png)  
These filters also works for [Clover](https://github.com/Floens/Clover) if you are using android.  

## ------>[Non-4chanx Filters CLICK HERE](https://gist.github.com/JustEndMyLife/cb9fa22b6b8b6362448f5c63040f62a6)<-------    
______________   

 Take off the [#] to toggle the optional filters  
____________________  


